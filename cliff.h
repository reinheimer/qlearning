#ifndef CLIFF_H
#define CLIFF_H

#include <stdint.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <gtk/gtk.h>

struct state {
  int l;
  int c;
};

// Actions
#define U         0
#define R         1
#define D         2
#define L         3
#define N_ACTIONS 4

// Strategies
#define RANDOM   -1
#define GREEDY    0
#define E_GREEDY  1
#define SOFTMAX   2

#define GAMA      0.9
#define EPSILON   0.1

// World
int **backups;  // backups matrix
int size_h;
int size_w;
unsigned int randval;
FILE *f;

// Agent
struct state initial_state;
int strategy;
float *q;  // expected reward
int max_ep;  // episodies
int max_st;  // steps per episode
double tal;
int mag;

void print_backups();
void print_q();
void init_backups();
void init_q();
void init(int h, int w, int pi);
int hash(struct state *s, int a);
int choose_action(struct state *s, int strategy);
int max_reward(struct state *next);
int emax_reward(struct state *s);
int softmax(struct state *s, double tal);
struct state *perform_action(struct state *s, int a);
float learn(int lives, int energy);
int main(int argc, char *argv[]);

#endif
