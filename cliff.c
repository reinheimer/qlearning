#include "cliff.h"
// interesting widgets
// GtkLabel
// GtkButton
// GtkHScale
// GktComboBoxText
// GtkEntry
// GtkStatusbar

void print_backups() {
  int i, j;
  for (i = 0; i < size_h; i++) {
    for (j = 0; j < size_w; j++)
      printf("%4.d ", backups[i][j]);
    printf("\n\n");
  }
}

void print_q() {
    int i, j, k;
    printf("\nLearned reward values\n\n");
    for (i = 0; i < size_h; i++) {
        for (j = 0; j < size_w; j++) {
            for (k = 0; k < N_ACTIONS / 2; k++) {
                // printf("%4.1f ", q[12 * i + 4 * j + k]);
                printf("%7.2f ", q[N_ACTIONS * (i * size_w + j) + k]);
            }
            if (j == size_w - 1) printf("\n");
            else printf("| ");
        }
        for (j = 0; j < size_w; j++) {
            for (k = N_ACTIONS / 2; k < N_ACTIONS; k++) {
                printf("%7.2f ", q[N_ACTIONS * (i * size_w + j) + k]);
            }
            if (j == size_w - 1) printf("\n");
            else printf("| ");
        }

        printf("\n\n");
    }
}

void init_backups() {
    int count, i, j;
    //
    backups = calloc(size_h, sizeof(int *));

    for (count = 0; count < size_h; count++)
      backups[count] = calloc(size_w, sizeof(int));

    for (i = 0; i < size_h; i++)
      for (j = 0; j < size_w; j++)
        backups[i][j] = -1;

    // Cliff
    for (count = 1; count < size_w - 1; count++)
      backups[size_h - 1][count] = -100;

    // Goal state
    backups[size_h - 1][size_w - 1] = 100;

    // print_backups();
}

void init_q() {
    q = (float *) calloc(size_w * size_h * N_ACTIONS, sizeof(float));
}

void init(int h, int w, int pi) {

    size_h = h;
    size_w = w;
    strategy = pi;

    init_backups();
    init_q();

    initial_state.l = size_h - 1;
    initial_state.c = 0;

    // srand(time(NULL));

    f = fopen("/dev/urandom", "r");

}

int hash(struct state *s, int a) {
    return N_ACTIONS * (s->l * size_w + s->c) + a;
    // return 12 * s->l + 4 * s->c + a;
}

int choose_action(struct state *s, int strategy) {

    int r;

    switch (strategy) {
      case RANDOM:
        fread(&randval, sizeof(randval), 1, f);
        // r = arc4random_uniform(4); // randval % 4;
        r = randval % 4;
        break;
      case GREEDY:
        r = max_reward(s);
        break;
      case E_GREEDY:
        r = emax_reward(s);
        break;
      case SOFTMAX:
        // how does temperature decay?
        tal = tal / (1 + pow(0.1, mag));
        r = softmax(s, tal);
        break;
    }

    return r;
}

int max_reward(struct state *next) {
    int i, max_a;
    float max_q, aux;

    fread(&randval, sizeof(randval), 1, f);
    // max_a = arc4random_uniform(4); // randval % 4;  // if there is a tie, choose at random
    max_a = randval % 4;
    max_q = q[hash(next, max_a)];

    // actions possible in next state
    for (i = 1; i < 4; i++) {
        if (next->l <= 0 && i == 0) continue;
        if (next->c >= size_w - 1 && i == 1) continue;
        if (next->l >= size_h - 1 && i == 2) continue;
        if (next->l <= 0 && i == 3) continue;

        aux = q[hash(next, i)];
        if (aux > max_q) {
            max_a = i;
            max_q = aux;
        }
    }
    return max_a;
}

int emax_reward(struct state *s) { // not Epsilon-decreasing strategy

    fread(&randval, sizeof(randval), 1, f);
    // int p = arc4random_uniform(10); // randval % 10;  // draw a number between 0 and 9
    int p = randval % 10;  // draw a number between 0 and 9

    if (p < 10 * (1 - EPSILON))
        return max_reward(s);  // (1 - e)% exploitation
    else return choose_action(s, RANDOM);  // e% exploration

}

int softmax(struct state *s, double tal) {
    // exp([q atual de s, a]/tal)
    // sobre
    // sum over all a de exp([q atual de s, a]/tal)
    int max_a;

    // fread(&randval, sizeof(randval), 1, f);
    // max_a = arc4random_uniform(4); // randval % 4;  // if there is a tie, choose at random
    // max_a = randval % 4;  // if there is a tie, choose at random
    // max_q = q[hash(s, max_a)];

    double *nums = (double *) calloc(N_ACTIONS, sizeof(double));
    double denom = 0;
    int i;

    // Calc probabilities
    for (i = 0; i < N_ACTIONS; i++) {
        // if (next->l <= 0 && i == 0) continue;
        // if (next->c >= size_w - 1 && i == 1) continue;
        // if (next->l >= size_h - 1 && i == 2) continue;
        // if (next->l <= 0 && i == 3) continue;
        nums[i] = exp(q[hash(s, i)] / tal);
        // printf("nums[%d] = %f\n", i, nums[i]);
        denom += nums[i];
    }

    for (i = 0; i < N_ACTIONS; i++) {
        if (denom > 0)
          nums[i] /= denom;
        // printf("%f ", nums[i]);
    }
    // printf("\n");

    fread(&randval, sizeof(randval), 1, f);

    double normalized = (randval) / ((double) UINT32_MAX);
    // printf("\n\n[%.2f %.2f %.2f %.2f] normalized = %.4f\n", nums[U], nums[R], nums[D], nums[L], normalized);

    if (normalized < nums[U])
        max_a = U;
    else if (normalized < nums[R] + nums[U])
        max_a = R;
    else if (normalized < nums[D] + nums[R] + nums[U])
        max_a = D;
    else max_a = L;

    /* // so wrong
    for (i = 1; i < 4; i++) {
        if (s->l <= 0 && i == 0) continue;
        if (s->c >= size_w - 1 && i == 1) continue;
        if (s->l >= size_h - 1 && i == 2) continue;
        if (s->l <= 0 && i == 3) continue;

        aux = q[hash(s, i)];
        if (aux > max_q) {
            max_a = i;
            max_q = aux;
        }
    }*/

    // printf("max_a = %d\n", max_a);

    return max_a;
}

struct state *perform_action(struct state *s, int a) {

    struct state *new;
    new = malloc(sizeof(struct state));
    memcpy(new, s, sizeof(struct state));
    // new->l = s->l;  // this never happened
    // new->c = s->c;

    switch (a) {
      case U:
        // printf("Passinho pra cima\n");
        if (new->l >= 1) new->l--;
        break;
      case R:
        // printf("Passinho pra direita\n");
        if (new->c < size_w - 1) new->c++;
        break;
      case D:
        // printf("Passinho pra baixo\n");
        if (new->l < size_h - 1) new->l++;
        break;
      case L:
        // printf("Passinho pra esquerda\n");
        if (new->c >= 1) new->c--;
        break;
    }

    return new;
}

float learn(int lives, int energy) {
    int a, max_a, t;
    float alpha;
    int passinhos = 0;
    float quantas = 0.0;
    max_st = energy;
    max_ep = lives;
    tal = 100000.0;

    // TODO: test convergence (there is no need to spend all lives)
    while (lives--) {
        struct state s = initial_state;

        energy = max_st;  // reset energy level
        t = 1.0;


        while (energy > 0) {
            alpha = 1.0 / t;

            a = choose_action(&s, strategy);  // use default strategy
            // printf("Chosen action %d\n", a);

            struct state *next_s = perform_action(&s, a);
            max_a = max_reward(next_s); // returns max(Q(s', a')) over all a'

            // int old_reward = q[hash(&s, a)]; // after 5000 hours of debugging
            float old_reward = q[hash(&s, a)];

            q[hash(&s, a)] = old_reward + alpha * (backups[next_s->l][next_s->c] + GAMA * q[hash(next_s, max_a)] - old_reward);

            // printf("Q(s, a) = %f + %f * (%d + %f * %f - %f)\n", old_reward, alpha, backups[next_s->l][next_s->c], GAMA, q[hash(next_s, max_a)], old_reward);
            // printf("Guardando %f na posicao q[%d]\n", q[hash(&s, a)], hash(&s, a));
            // printf("Next state %d %d - Energy level: %d\n\n", s.l, s.c, energy);

            if (backups[next_s->l][next_s->c] == 100) {
                // printf("GOAL! Lives remaining: %d\n", lives);
                quantas++;
                passinhos += t;
                break; // found the gold bucket!
            }

            // if (next_s->l == 3 && next_s->c == size_w - 1) break;
            if (backups[next_s->l][next_s->c] <= -100) break; // fell from the cliff

            energy += backups[s.l][s.c];

            s = *next_s;
            t++;
        }
    }

    // if the goal was reached at least once, returns the average of steps taken
    if (quantas) {
        // printf("\n\nPassinhos: %d, quantas: %f, %f\n\n", passinhos, quantas, passinhos / quantas);
        return (passinhos / quantas);
    } else return 0.0;
    print_q();
}


int main(int argc, char *argv[]) {
  int i = 1, m, k;
  /*
  GtkWidget *window;
  GtkWidget *align;

  GtkWidget *lbl;

  gtk_init(&argc, &argv);



  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(window), "GtkAlignment");
  gtk_window_set_default_size(GTK_WINDOW(window), 300, 200);
  gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
  gtk_container_set_border_width(GTK_CONTAINER(window), 5);

  align = gtk_alignment_new(0, 1, 0, 0);

  gchar *str = "<b>ZetCode</b>, knowledge only matters";
  lbl = gtk_label_new(NULL);
  gtk_label_set_markup(GTK_LABEL(lbl), str);

  gtk_container_add(GTK_CONTAINER(align), lbl);
  gtk_container_add(GTK_CONTAINER(window), align);

  g_signal_connect(G_OBJECT(window), "destroy",
      G_CALLBACK(gtk_main_quit), NULL);

  gtk_widget_show_all(window);

  // gtk_main();
  */
  float quantas;

  printf("Lives\tTal decay\tr1\tr2\tr3\tr4\tr5\tAvg\tSucesses\n");

  mag = 2;

  while (i < 1000) {
      // for (m = 0; m < 6; m++) {
          // accum = 0;
          // mag = m;
          printf("%d\t%d\t", i, mag);
          // printf("Trying with %d lives (tal decresing with magnitude %d) = ", i, m);
          for (k = 0; k < 5; k++) {  // 5 trials
              init(4, 12, SOFTMAX);
              quantas = learn(i, 18); // average of steps taken
              printf("%.4f\t", quantas);
              fclose(f);
              //accum += quantas;
              // printf("%d ", quantas);
          }

          // printf(" - Average: %.2f\n", accum / 5.0);
          printf("=IF(COUNTIF(C%d:G%d;\">0\")>0;SUM(C%d:G%d)/COUNTIF(C%d:G%d;\">0\");0)\t=COUNTIF(C%d:G%d;\">0\")\n", i + 1, i + 1, i + 1, i + 1, i + 1, i + 1, i + 1, i + 1);
          // printf("tal = %f\n", tal);
      // }

      //printf("\n");
      i++;
  }


  return 0;
}
